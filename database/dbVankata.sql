Create database dbVankata
go

use dbVankata 
go

CREATE TABLE Parts_GMM_04 (
    ID INT PRIMARY KEY IDENTITY(1,1),
    Last_Modified VARCHAR(255),
    Shipment_ID VARCHAR(255),
    B_L_Origin VARCHAR(255),
    B_L_Destination VARCHAR(255),
    Consignee_Reference VARCHAR(255),
    SP_Ref_1 VARCHAR(255),
    Equipment_Summary VARCHAR(255),
    Earliest_Ship_Date VARCHAR(255),
    Group_Name VARCHAR(255)
);

CREATE TABLE Carrier_Mismatch (
    ID INT PRIMARY KEY IDENTITY(1,1),
    Booking_Number VARCHAR(255),
    Carrier VARCHAR(255),
    Consignee_Ref VARCHAR(255),
    Forwarder_Ref VARCHAR(255),
    Code VARCHAR(255),
    Created_By VARCHAR(255)
);

CREATE TABLE CaterpillarBookings (
    ID INT PRIMARY KEY IDENTITY(1,1),
    Last_Modified VARCHAR(255),
    Status VARCHAR(255),
    Shipment_ID VARCHAR(255),
    Commodity VARCHAR(255),
    Load_Type VARCHAR(255),
    B_L_Origin VARCHAR(255),
    B_L_Destination VARCHAR(255),
    Consignee_Reference VARCHAR(255)
);

CREATE TABLE Report_Kim (
    ID INT PRIMARY KEY IDENTITY(1,1),
    Status VARCHAR(255),
    Transaction_Number VARCHAR(255),
    Booking_Number VARCHAR(255),
    Submit_Date VARCHAR(255),
    Code VARCHAR(255),
    Created_By VARCHAR(255),
    Consignee_Ref VARCHAR(255),
    Doc_Cut_Off VARCHAR(255),
    Carrier VARCHAR(255),
    Origin VARCHAR(255),
    Destination VARCHAR(255),
    Port_of_Load VARCHAR(255),
    First_Vessel_Voyage_Flag VARCHAR(255),
    Second_Vessel_Voyage_Flag VARCHAR(255),
    Service_String VARCHAR(255),
    Estimated_Departure_Place_of_Receipt VARCHAR(255),
    Earliest_Departure_Place_of_Receipt VARCHAR(255),
    Cut_off_Place_of_Receipt VARCHAR(255),
    Estimated_Departure_Port_of_Load VARCHAR(255),
    Actual_Departure_Port_of_Load VARCHAR(255),
    Estimated_Arrival_Port_of_Discharge VARCHAR(255),
    Actual_Arrival_Port_of_Discharge VARCHAR(255),
    Last_Event VARCHAR(255),
    Last_Updated VARCHAR(255),
    Total_TEUs VARCHAR(255),
    Consignee VARCHAR(255),
    Port_of_Discharge VARCHAR(255),
    Latest_Arrival_Place_of_Delivery VARCHAR(255),
    Ship_To VARCHAR(255),
    Port_of_Discharge_UNLOCODE VARCHAR(255),
    Cntrs_Ingated VARCHAR(255),
    Cntrs_Moving VARCHAR(255),
    Cntrs_Manifested VARCHAR(255),
    Cntrs_Booked VARCHAR(255),
    Shipment_Plan_ID VARCHAR(255),
    [Group] VARCHAR(255)
);

CREATE TABLE ViewBookings (
    ID INT PRIMARY KEY IDENTITY(1,1),
    Last_Modified VARCHAR(255),
    Status VARCHAR(255),
    Shipment_ID VARCHAR(255),
    Commodity VARCHAR(255),
    Load_Type VARCHAR(255),
    B_L_Origin VARCHAR(255),
    B_L_Destination VARCHAR(255),
    Booking_Number VARCHAR(255),
    Consignee_Reference VARCHAR(255),
    SP_Ref_1 VARCHAR(255),
    Equipment_Summary VARCHAR(255),
    Earliest_Ship_Date VARCHAR(255),
    Provider VARCHAR(255),
    Est_Departure_BL_Origin VARCHAR(255),
    Est_Departure_Port_of_Load VARCHAR(255),
    Est_Arrival_Port_of_Discharge VARCHAR(255),
    Est_Time_of_Arrival_BL_Destination VARCHAR(255),
    Booking_Status VARCHAR(255)
);

