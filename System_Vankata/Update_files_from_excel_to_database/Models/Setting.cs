﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Update_files_from_excel_to_database.Models
{
    internal class Setting
    {
        public string Origen { get; set; }
        public string Credenciales { get; set; }
        public string Nombre { get; set; }
        public string Columnas { get; set; }
        public string Conexion { get; set; }
        public string Tabla { get; set; }
        public string ErrorEmail { get; set; }
    }
}
