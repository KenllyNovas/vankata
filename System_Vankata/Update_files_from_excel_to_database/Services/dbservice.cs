﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Update_files_from_excel_to_database.Models;

namespace Update_files_from_excel_to_database.Services
{
    internal static class dbservice
    {

        // Método de inserción para la clase CarrierMismatch
        public static void InsertData(Dictionary<string, List<object>> excel, string tableName, string cadenaConexion)
        {
            try
            {
                int totalRow = excel.Values.Select(x => x.Count).FirstOrDefault();
                string patron = "[\\s/]";
                string consulta = string.Empty;

                for (int i = 0; i < totalRow; i++)
                {
                    string datos = string.Empty;
                    string header = string.Empty;

                    consulta += "INSERT INTO " + tableName + "";
                    foreach (var item in excel)
                    {
                        // concatenando columnas
                        string key = "[" + item.Key + "]";
                        header += Regex.Replace(key, patron, "_") + ",";

                        // Concatenando valores
                        var fila = excel[item.Key][i];
                        datos += "'" + (fila ==null ? "" : fila.ToString().Replace("'", "''")) + "',";


                    }
                    

                    consulta += "(" + header.Remove(header.Length - 1) + ") values (" + datos.Remove(datos.Length - 1) + ");";
                }
                //Console.WriteLine(consulta);
                //"VALUES (@Dato1, @Dato2)";

                // Crear y abrir conexión
                using (SqlConnection conexion = new SqlConnection(cadenaConexion))
                {
                    conexion.Open();

                    // Crear comando SQL con parámetros
                    using (SqlCommand comando = new SqlCommand(consulta, conexion))
                    {
                        // Ejecutar comando de inserción
                        comando.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
