﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Update_files_from_excel_to_database.Services
{
    internal static class EmailService
    {
        public static bool SendEmail(string Body,string email)
        {
            string server = ConfigurationManager.AppSettings["Server"];
            string username = ConfigurationManager.AppSettings["EmailSender"];
            string password = ConfigurationManager.AppSettings["Password"];
            //int port = Convert.ToInt16(ConfigurationManager.AppSettings["Port"]);
            bool UseSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["UseSsl"]);

            MailMessage mailMessage = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient(server);
            //SmtpServer.Port = port;
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.Credentials = new NetworkCredential(username, password);
            SmtpServer.EnableSsl = UseSsl;

            mailMessage.From = new MailAddress(username);

            foreach (var item in email.Split(';'))
            {
                mailMessage.To.Add(item);
            }
           

            mailMessage.Subject = ConfigurationManager.AppSettings["Subject"];
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = "<p><strong>" + Body + "</strong></p>";

            SmtpServer.Send(mailMessage);
            return true;
        }
    }
}
