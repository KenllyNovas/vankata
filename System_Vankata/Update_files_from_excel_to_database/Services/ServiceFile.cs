﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Pipes;
using System.Linq;

namespace Update_files_from_excel_to_database.Services
{
    internal static class ServiceFile
    {

        static ServiceFile()
        {

        }

        public static Dictionary<string, List<object>> LoadDataExcel(string path, string[] columns)
        {
            try
            {
                using (var stream = File.Open(path, FileMode.Open, FileAccess.Read))
                {
                    IExcelDataReader excelData;
                    if (Path.GetExtension(path).ToLower() == ".xls")
                    {
                        //1.1 Reading from a binary Excel file ('97-2003 format; *.xls)
                        excelData = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else
                    {
                        //1.2 Reading from a OpenXml Excel file (2007 format; *.xlsx)
                        excelData = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }

                    using (var reader = excelData)
                    {
                        var conf = new ExcelDataSetConfiguration()
                        {
                            ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true //THIS IS WHAT YOU ARE AFTER
                            }
                        };

                        var result = reader.AsDataSet(conf);

                        // Acceso a datos
                        DataTable table = result.Tables[0];
                        Dictionary<string, List<object>> excelFile = new Dictionary<string, List<object>>();

                        for (int i = 0; i < table.Columns.Count; i++)
                        {
                            if (columns.Contains(table.Columns[i].ColumnName))
                            {
                                var row = table.AsEnumerable().Select(r => r.Field<object>(i)).ToList();

                                //almacenamos las columnas con sus filas
                                excelFile.Add(table.Columns[i].ColumnName, row);
                            }

                        }

                        return excelFile;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
