﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using Update_files_from_excel_to_database.Models;

namespace Update_files_from_excel_to_database.Services
{
    internal static class ServiceConfig
    {
        public static List<Setting> GetSettings()
        {
            try
            {
                // Obtener la ruta relativa del archivo dentro del proyecto
                string fileName = "Setting.json";

                // Combinar la ruta de la carpeta raíz del proyecto con la ruta relativa del archivo
                string filePath = Path.Combine(Environment.CurrentDirectory, fileName);

                // Verificar si el archivo existe
                if (File.Exists(filePath))
                {
                    // Lee el contenido del archivo JSON
                    string jsonString = File.ReadAllText(filePath);

                    var forecastNode = (JObject)JsonConvert.DeserializeObject(jsonString);
                    List<Setting> settings = new List<Setting>();

                    //var jsonArray = forecastNode;
                    foreach (var item in forecastNode)          
                        settings.Add(JsonConvert.DeserializeObject<Setting>(item.Value.ToString()));
                    

                    return settings;
                }
                else
                {
                    throw new Exception("El archivo de configuración no existe.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
