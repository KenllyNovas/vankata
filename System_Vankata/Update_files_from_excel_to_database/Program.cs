﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Update_files_from_excel_to_database.Models;
using Update_files_from_excel_to_database.Services;

namespace Update_files_from_excel_to_database
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leyendo configuraciones");
            List<Setting> settings = ServiceConfig.GetSettings();

            if (settings != null)
            {
                foreach (Setting setting in settings)
                {
                    try
                    {

                        /*
                            Aqui irá la sección de buscar el archivo en el servidor
                         */

                        Console.WriteLine("Leyendo archivo: " + setting.Nombre);       

                        //Cargando los valores desde el excel
                        var files = Directory.EnumerateFiles(setting.Origen, "*.*");
                        string pathFile = files.Where(x => x.Contains(setting.Nombre)).FirstOrDefault();
                        string[] columns = setting.Columnas.Split(',');

                        Console.WriteLine("Cargando en memoria archivo: " + setting.Nombre);
                        var memoryExcel = ServiceFile.LoadDataExcel(pathFile, columns);

                        Console.WriteLine("Insertando registros a la base de datos archivo: " + setting.Nombre);
                        dbservice.InsertData(memoryExcel, setting.Tabla, setting.Conexion);
                        Console.WriteLine("Carga de archivos finalizada.");

                    }
                    catch (Exception ex)
                    {
                        EmailService.SendEmail("Error generado archivo: " + setting.Nombre + " Error:" + ex.Message, setting.ErrorEmail);
                        Console.WriteLine("Enviando correo debido a error generado archivo. " + setting.Nombre + "");
                    }
                }
            }
            else
                Console.WriteLine("No se encontraron archivos para exportar.");


            Console.ReadLine();
        }
    }
}
